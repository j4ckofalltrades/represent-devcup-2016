import firebase from 'firebase';

export default function($log, $firebaseObject, $firebaseArray, constants, $q) {

	var vm = this;
	var isLoggedIn = false;
	var user = null;

	function pushToCollection(collectionName, data) {
		let mtRef = firebase.database().refFromURL(constants.FIREBASE_DB_URL + "/" + collectionName);
		mtRef.push(data)
  		return $firebaseArray(mtRef);
	}

	function findEqualElementByParam(collectionName, param, value) {
		// Return the key of an element in a collection depending on the parameter
		var deferred = $q.defer();
		var ref = firebase.database().ref(collectionName);
		ref.orderByChild(param).equalTo(value).on("child_added", function(snapshot) {
			console.log(typeof snapshot.key);
		  deferred.resolve(snapshot.val());
		});
		return deferred.promise;
	}

	function getCollectionRef(collectionName) {
		// Return the key of an element in a collection depending on the parameter
		var ref = firebase.database().ref(collectionName);
		return ref;
	}

	function queryCollection(collectionName, param, value) {
		// Return the key of an element in a collection depending on the parameter
		var ref = firebase.database().ref(collectionName).orderByChild(param).equalTo(value);
		return ref;
	}

	function checkIfUserExists(user) {
		var deferred = $q.defer();
		var ref = firebase.database().ref(constants.USER_COLLECTION);
		ref.orderByChild('username').equalTo(user.username).on("child_added", function(snapshot) {
			var retrieved_user = snapshot.val();
			console.log(retrieved_user);
			if(retrieved_user == ''){
				deferred.resolve(retrieved_user)
			} else {
				if(retrieved_user.password == user.password) {
					console.log("successfully login");
					deferred.resolve(retrieved_user);
				} else {
					console.log("invalid");
					deferred.resolve(retrieved_user);
				}
			}
		});
		return deferred.promise;
	}


	return {
		isLoggedIn : isLoggedIn,
		user : user,
		getCollectionRef: getCollectionRef,
	    pushToCollection: pushToCollection,
	    findEqualElementByParam : findEqualElementByParam,
	    checkIfUserExists : checkIfUserExists,
			queryCollection : queryCollection
	};
}
