import firebase from 'firebase';
import moment from 'moment';
import swal from 'sweetalert2';
import 'sweetalert2/dist/sweetalert2.css';

export default function($log, $stateParams, firebaseService, $q, $firebaseArray, $state) {
  var vm = this;

  vm.calendarView = 'month';
  vm.viewDate = new moment().toDate();
  vm.cellModifier = calculateCellClass;
  vm.onTimespanClick = onTimespanClick;
  vm.addParticipant = addParticipant;
  vm.bookHike = bookHike;

  vm.events = [];

  vm.mountainId = $stateParams.mountainId;
  var mountainRef = firebaseService.getCollectionRef('mountains');
  var mountainArray = $firebaseArray(mountainRef);
  mountainArray.$loaded().then(function(list){
    vm.mountain = list.$getRecord(vm.mountainId);
  });

  vm.hikeForm = {
    duration: 'single',
    startDate: moment().toDate(),
    participants: [
      {name:'You', removable:false}
    ]
  };

  init();

  function init(){
    var query = firebaseService.queryCollection('bookings', 'mountainId', vm.mountainId);
    query.on("child_added", function(messageSnapshot) {
      var value = messageSnapshot.val();
      if(value.startDate){
        value.startsAt = moment(value.startDate).toDate();
      }
      if(value.endDate){
        value.endsAt = moment(value.endDate).toDate();
      }
      // This will only be called for the last 100 messages
      vm.events.push(value);
    });
  }
  function calculateCellClass(cell) {
    var capacity = vm.mountain.capacity;
    var total = 0;
    if(cell.events){
      for(let i = 0; i < cell.events.length; i++){
        total += cell.events[i].numPersons || 0;
      }
    }
    var ratio = capacity > 0 ? (Math.round(total * 100.0 / capacity) / 100): 0.0;
    //Calculate class proper
    if(ratio < 0.5){
      //do nothing
    }else if(ratio < 0.8){
      cell.cssClass = 'capacity-caution';
    }else{
      cell.cssClass = 'capacity-warning';
    }
    return;
  }

  function onTimespanClick(date){
    vm.hikeForm.startDate = date;
  }

  function addParticipant(){
    vm.hikeForm.participants.push({
      name:'Friend', removable:true
    });
  }

  function bookHike(){
    firebaseService.pushToCollection('bookings', transformFormDataToPayload(vm.mountainId, vm.hikeForm));
    swal("Book Success","You have successfully booked your hike!","success").then(function(){
      $state.go('profile');
    });
  }

  function transformFormDataToPayload(mountainId, formData){
    var payload = {};
    payload.mountainId = mountainId;
    payload.startDate = moment(formData.startDate).format();
    if(formData.duration == 'multiple'){
      payload.endDate = moment(formData.endDate).format();
    }
    payload.numPersons = formData.participants.length;
    console.log(payload);
    return payload;
  }
  /*$log.info('test dawi');
  let ref = firebase.database().ref();
  let syncObject = $firebaseObject(ref);

  syncObject.$bindTo(vm, 'data');*/
}
