import ctrl from './bookHikeController';

export default (ngModule) =>
  ngModule.component('bookHike', {
    template: require('./bookHike.html'),
    controller: ctrl
  });
