import angular from 'angular';
import moment from 'moment';
import bootstrapCalendar from 'angular-bootstrap-calendar';
import 'angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.min.css';

import component from './bookHikeComponent';

const MODULE_NAME = 'bookHikeModule';

let module = angular.module(MODULE_NAME, [
  bootstrapCalendar
]);

component(module);

export default module;
