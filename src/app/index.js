import jQuery from 'jquery';
import angular from 'angular';
import uiRouter from 'angular-ui-router';
import firebase from 'firebase';
import angularfire from 'angularfire';
import angular_animate from 'angular-animate';
import angular_aria from 'angular-aria';
import angular_material from 'angular-material';
import moment from 'moment';
import 'angular-material/angular-material.min.css';

import routes from './routes';
import service from './service';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap-theme.min.css';
import '../style/app.css';

import test from './test/index';
import bookHike from './bookHike/index';
import headerBar from './headerBar/index';
import {constants} from './constants.js';
import listMountains from './listMountains/index';
import profile from './profile/index';

// init firebase
let config = {
  apiKey: constants.API_KEY,
  authDomain: constants.AUTH_DOMAIN,
  databaseURL: constants.FIREBASE_DB_URL,
  storageBucket: constants.STORAGE_BUCKET,
};
firebase.initializeApp(config);

const MODULE_NAME = 'app';

let app = angular.module(MODULE_NAME, [
    test.name,
    bookHike.name,
    headerBar.name,
    listMountains.name,
    profile.name,
    uiRouter,
    angularfire,
    angular_animate,
    angular_aria,
    angular_material
  ]);
app.factory('firebaseService', service);
app.constant('constants', constants);
app.config(function($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('blue')
    .accentPalette('amber')
    .warnPalette('amber');
  $mdThemingProvider.theme('inverse')
    .backgroundPalette('blue')
    .dark();
});
app.run(function($log) {
  $log.info('The application has been initialized.');
});

routes(app);


angular.bootstrap(document, [app.name]);
