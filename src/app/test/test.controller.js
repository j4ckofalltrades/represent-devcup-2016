import firebase from 'firebase';

export default function($log, $firebaseObject, $firebaseArray, $scope) {
  $log.info('test controller init');
  let ref = firebase.database().ref();
  let syncObject = $firebaseObject(ref);
  syncObject.$bindTo($scope, 'data');

  let mtRef = firebase.database().refFromURL("https://represent-devcup-2016.firebaseio.com/mountains");
  mtRef.push({'name' : 'Mt. Amuyao', 'elev' : '2702m'});
  mtRef.push({'name' : 'Mt. Pulag', 'elev' : '2922m'});
  mtRef.push({'name' : 'Mt. Ugo', 'elev' : '2150m'});
  $scope.mountains = $firebaseArray(mtRef);
}
