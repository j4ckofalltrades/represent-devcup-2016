import testCtrl from './test.controller';

export default (ngModule) =>
  ngModule.component('test', {
    template: require('./test.html'),
    controller: testCtrl
  });
