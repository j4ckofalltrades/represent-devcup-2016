import angular from 'angular';

import component from './test.component';

const MODULE_NAME = 'test';

let test = angular.module(MODULE_NAME, []);

component(test);

export default test;
