import firebase from 'firebase';

export default function (constants, $firebaseArray, $state, firebaseService) {
    var vm = this;

    vm.mountains = [];
    vm.isLoggedIn = function(){
      return firebaseService.isLoggedIn;
    };

    let mountainsRef = firebase.database().refFromURL(constants.FIREBASE_DB_URL + '/mountains');
    vm.mountains = $firebaseArray(mountainsRef);

    vm.bookHike = function (object) {
        $state.go('bookHike', {'mountainId': object.$id})
    };

    vm.toggleExpand = function (object) {
        object.expand = !object.expand;
    };
}
