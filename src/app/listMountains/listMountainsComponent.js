import ctrl from './listMountainsController';

export default (ngModule) =>
    ngModule.component('listMountains', {
        template: require('./listMountains.html'),
        controller: ctrl
    });