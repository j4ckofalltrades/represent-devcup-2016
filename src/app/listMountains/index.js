import angular from 'angular';

import component from './listMountainsComponent';

const MODULE_NAME = 'listMountainsModule';

let module = angular.module(MODULE_NAME, [
]);

component(module);

export default module;
