import profileCtrl from './profile.controller';

export default (ngModule) =>
    ngModule.component('profile', {
        template: require('./profile.html'),
        controller: profileCtrl
    });