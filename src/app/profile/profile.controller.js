import firebase from 'firebase';

export default function($log, $firebaseArray, firebaseService, $stateParams) {

  var vm = this;
  vm.name = firebaseService.user.firstName + " " + firebaseService.user.lastName;
  vm.email = firebaseService.user.email;
  vm.birthday = firebaseService.user.birthday;
  vm.username = firebaseService.user.username;

  vm.events = [
  	{
  		what : "Climb 1",
  		when : "12/22/2016",
  		description : "Pre-Climb Training"
  	},
  	{
  		what : "Climb 2",
  		when : "01/01/2017",
  		description : "New year Climb"
  	},
  ];
  // vm.showDarkTheme = true;
     

}
