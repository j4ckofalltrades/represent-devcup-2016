import angular from 'angular';

import component from './profile.component';

const MODULE_NAME = 'profileModule';

let module = angular.module(MODULE_NAME, [
]);

component(module);

export default module;
