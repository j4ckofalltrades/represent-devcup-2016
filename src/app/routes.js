export default (ngModule) => ngModule.config(routes);

function routes($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/listMountains');
    $stateProvider
        .state('generate', {
            url: '/generate',
            template: '<test/>'
        })
        .state('bookHike', {
            url: '/book/:mountainId',
            template: '<book-hike/>'
        })
        .state('listMountains', {
            url: '/listMountains',
            template: '<list-mountains/>'
        })
        .state('profile', {
            url: '/profile',
            template: '<profile/>'
        });
}
