import LoginCtrl from './login.controller';
import SignupCtrl from './signup.controller';

export default function($log, $mdDialog, firebaseService, $state) {
  var vm = this;

  vm.openLoginModal = openLoginModal;
  vm.openSignupModal = openSignupModal;
  vm.isLoggedIn = isLoggedIn;
  vm.goToProfile = goToProfile;
  vm.goToHome = goToHome;

  console.log("HEADERBAR", vm.isLoggedIn);

  function goToProfile() {
    $state.go('profile');
  }

  function goToHome() {
    $state.go('listMountains');
  }
  
  function isLoggedIn() {
    return firebaseService.isLoggedIn;
  }

  function openLoginModal(ev) {
    $mdDialog.show({
      controller: LoginCtrl,
      controllerAs: 'vm',
      templateUrl: 'loginModal.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:false
      // fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    })
    .then(function() {
      $log.info('You clicked OK.');
    }, function() {
      $log.info('You cancelled the dialog.');
    });
  }

  function openSignupModal(ev) {
    $mdDialog.show({
      controller: SignupCtrl,
      controllerAs: 'vm',
      templateUrl: 'signupModal.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:false
      // fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    })
    .then(function() {
      $log.info('You clicked OK.');
    }, function() {
      $log.info('You cancelled the dialog.');
    });
  }
}
