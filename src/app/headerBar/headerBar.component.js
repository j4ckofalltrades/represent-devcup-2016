import headerCtrl from './headerBar.controller';

export default (ngModule) =>
  ngModule.component('headerBar', {
    template: require('./headerBar.html'),
    controller: headerCtrl
  });
