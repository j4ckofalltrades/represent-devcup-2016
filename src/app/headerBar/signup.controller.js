
export default function($log, $mdDialog, $mdToast, firebaseService, constants, $state) {
  var vm = this;

  vm.user = {
    username : null,
    firstname : null,
    lastname : null,
    email : null,
    bday : null
  };

  vm.handleSubmit = handleSubmit;
  vm.handleCancel = handleCancel;


  function handleSubmit() {

    firebaseService.pushToCollection(constants.USER_COLLECTION, vm.user);
    firebaseService.user = vm.user;
    firebaseService.isLoggedIn = true;
    $state.go('listMountains', {'user': vm.user});

    return $mdDialog.hide();
  	  
  }

  function showSignupError(message) {
    var toast = $mdToast.simple()
      .textContent(message)
      .action('CLOSE')
      .highlightAction(true)
      .highlightClass('md-accent')// Accent is used by default, this just demonstrates the usage.
      .position("top");
    $mdToast.show(toast).then(function(response) {
    });
  }

  function handleCancel() {
  	return $mdDialog.hide();
  }
}
