import angular from 'angular';

import component from './headerBar.component';

const MODULE_NAME = 'headerBar';

let headerBar = angular.module(MODULE_NAME, []);

component(headerBar);

export default headerBar;
