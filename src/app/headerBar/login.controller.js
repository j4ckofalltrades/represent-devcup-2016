
export default function($log, $mdDialog, firebaseService, constants, $mdToast, $state) {
  var vm = this;

  vm.user = {
    username : null,
    password : null
  };

  vm.handleSubmit = handleSubmit;
  vm.handleCancel = handleCancel;

  function handleSubmit() {
    firebaseService.checkIfUserExists(vm.user).then(function(response){
      if(response != '') {
        firebaseService.isLoggedIn = true;
        firebaseService.user = response;
        $state.go('listMountains', {'user': response});
      }
      else{
        showLoginError();
      }
    });
          
  	return $mdDialog.hide();
  }

  function handleCancel() {
    $log.info();
  	return $mdDialog.hide();
  }

  function showLoginError() {
    var toast = $mdToast.simple()
      .textContent('Invalid credentials')
      .action('CLOSE')
      .highlightAction(true)
      .highlightClass('md-accent')// Accent is used by default, this just demonstrates the usage.
      .position("top");
    $mdToast.show(toast).then(function(response) {
    });
  }

}
