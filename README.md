# devcup-2016

## Quick start

> Clone/Download the repo then edit `index.js` inside `/src/app/index.js`

```bash
# install the dependencies with npm
$ npm install

# start the server
$ npm start

```

## Authors

* Jordan Duabe
* Darryl John Ong
* Jasmin Caronan

> **devcup-2016** © 2016+
